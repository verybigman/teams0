Feature: Manage Teams
	In order to make a team
	As an user
	I want to create and manage teams

	@base @create
	Scenario: Teams List
		Given I have teams named Team A, Team B
		When I visit the teams page
		Then I should see Team A
		And I should see Team B

	@valid @create
	Scenario: Create Valid Team
		Given I have no teams
		And I have countries named Moldova, Russia
		And I am on the teams page
		When I follow "Add new team"
		And I select Moldova from "team" "country_id"
		And I fill in "team" "name" with Team A
		And I fill in "team" "count" with 20
		And I fill in "team" "work_list" with Painting, Building, Cleaning
		And I fill in "team" "cost" with 100.0
		And I press "OK"
		Then I should see New team added successfully!
		And I should see Moldova
		And I should see Team A
		And I should see 20
		And I should see Painting
		And I should see Building
		And I should see Cleaning
		And I should see 100.0
		And i should have 1 team

	@show
	Scenario: Show Team
		Given I have teams named Team A, Team B
		And I am on the teams page
		When I follow "Team A"
		Then I should see Team A

	@valid @edit
	Scenario: Edit Team
		Given I have teams named Team A
		And I have countries named Moldova, Russia
		And I am on the "Team A" page
		When I follow "Edit team"
		And I select Moldova from "team" "country_id"
		And I fill in "team" "name" with Team A
		And I fill in "team" "count" with 20
		And I fill in "team" "work_list" with Painting, Building, Cleaning
		And I fill in "team" "cost" with 100.0
		And I press "OK"
		Then I should see Team updated successfully!
		And I should see Moldova
		And I should see Team A
		And I should see 20
		And I should see Painting
		And I should see Building
		And I should see Cleaning
		And I should see 100.0

	@sorting
	Scenario: Sort by cost, default ASC, on first click is DESC
		Given I have team named Team A with cost 50
		And I have team named Team B with cost 100
		And I am on the home page
		When I follow "Teams"
		Then I should see Team A
		And I should see Team B
		And 50 should appear before 100
		Then I follow "Cost"
		And 100 should appear before 50

	@sorting
	Scenario: Sort by count, default none, on first click is DESC
		Given I have team named Team A with count 10
		And I have team named Team A with count 20
		And I am on the teams page
		When I follow "People"
		Then 20 should appear before 10
		Then I follow "People"
		And 10 should appear before 20

	# for use this test, please, download the chromedriver from https://code.google.com/p/chromedriver/downloads/list
	# cp chromedriver to /usr/bin
	# sudo chmod +x /usr/bin/chromedriver
	# uncomment this scenario and exec cucumber
	# @javascript @autocomplete
	# Scenario: Create Valid Team With Autocomplete
	# 	Given I have no teams
	# 	And I have countries named Moldova, Russia
	# 	And the following works exists Painting, Building, Cleaning
	# 	And I am on the teams page
	# 	When I follow "Add new team"
	# 	And I select Moldova from "team" "country_id"
	# 	And I fill in "team" "name" with Team A
	# 	And I fill in "team" "count" with 20
	# 	And I fill in "team" "work_list" with Pai
	# 	And I choose "Painting" in the autocomplete list
	# 	And I fill in "team" "cost" with 100.0
	# 	And I press "OK"
	# 	Then I should see New team added successfully!
	# 	And I should see Moldova
	# 	And I should see Team A
	# 	And I should see 20
	# 	And I should see Painting
	# 	And I should see 100.0
	# 	And i should have 2 team

	# for use this test, please, download the chromedriver from https://code.google.com/p/chromedriver/downloads/list
	# cp chromedriver to /usr/bin
	# sudo chmod +x /usr/bin/chromedriver
	# uncomment this scenario and exec cucumber
	# @javascript @filtering
	# Scenario: Filter by country, default none
	# 	Given I have countries named Moldova, Russia
	# 	And I have team Team A in country Moldova
	# 	And I have team Team B in country Russia
	# 	And I am on the teams page
	# 	When I follow "Country"
	# 	Then I follow "Moldova"
	# 	And I should see Team A
	# 	And I should not see Team B
	# 	Then I follow "Country"
	# 	Then I follow "Russia"
	# 	And I should see Team B
	# 	And I should not see Team A

	# for use this test, please, download the chromedriver from https://code.google.com/p/chromedriver/downloads/list
	# cp chromedriver to /usr/bin
	# sudo chmod +x /usr/bin/chromedriver
	# uncomment this scenario and exec cucumber
	# @javascript @filtering
	# Scenario: Filter by works, first is most taggable
	# 	Given I have team Team A with works Painting, Building, Testing 
	# 	And I have team Team B with works Painting, Cleaning, Trashing
	# 	And I am on the teams page
	# 	When I follow "Work types"
	# 	Then I fill in "" "value" with Painting, Building, Tes
	# 	And I choose "Testing" in the autocomplete list
	# 	Then I press "Show"
	# 	And I should see Team A
	# 	And I should see Team B
	# 	And Team A should appear before Team B
	# 	Then I follow "Work types"
	# 	And I fill in "" "value" with Painting, Tra
	# 	And I choose "Trashing" in the autocomplete list
	# 	Then I press "Show"
	# 	And I should see Team A
	# 	And I should see Team B
	# 	And Team B should appear before Team A






