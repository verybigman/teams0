Feature: Manage Welcome
	In order to select model
	As an collaborator
	I want to select model for manage 

	@routing
	Scenario: Select teams
		Given I am on the home page
		When I follow "Teams"
		Then I will be on page of teams

	@routing
	Scenario: Select countries
		Given I am on the home page
		When I follow "Countries"
		Then I will be on page of countries

	@routing
	Scenario: Select works
		Given I am on the home page
		When I follow "Works"
		Then I will be on page of works