Feature: Manage Works
	In order to see all works
	As an user
	I want to see teams with required works

	@base
	Scenario: Works list
		Given I have team Team A with works Painting
		And I am on the teams page
		When I follow "Works"
		Then I see content Painting in table column name
		And I see content 1 in table column teams_count

	Scenario: Works links
		Given I have team Team A with works Painting, Building
		And I have team Team B with works Cleaning, Building
		And I am on the works page
		When I follow "Painting"
		Then I should see Team A
		And I should see Painting
		Then I visit the works page
		And I follow "Cleaning"
		And I should see Team B
		Then I visit the works page
		And I follow "Building"
		And I should see Team A
		And I should see Team B



