Given /^I have teams named (.+)$/ do |names|
  names.split(', ').each do |name|
    Team.create!(name: name)
  end
end

Given /^I have team (.+) with works (.+)$/ do |name, works|
    Team.create!(name: name, work_list: works)
end

Given /^I have team (.+) with cost (.+)$/ do |name, cost|
    Team.create!(name: name, cost: cost)
end

Given /^I have team (.+) with count (.+)$/ do |name, count|
    Team.create!(name: name, count: count)
end

Given /^I have team (.+) in country (.+)$/ do |name, country|
    country_id = Country.find_by_name(country).id
    Team.create!(name: name, country_id: country_id)
end

Given /^I have no teams$/ do
  Team.delete_all
end

Given /^the following works exists (.+)$/ do |works|
	@team = Team.create(name: "Example")
	@team.work_list = works
	@team.save!
end

Then /^i should have (\d+) team$/ do |count|
  Team.count.should == count.to_i
end

Given /^I am on the "([^\"]*)" page$/ do |team|
	visit team_path(Team.find_by_name(team))
end

Given /^I am on the new team page$/ do
	visit new_team_path
end

Then /(.+) should appear before (.+)$/ do |first, second|
    page.body.should =~ /#{first}.*#{second}/m
end