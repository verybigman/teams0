# Можно сделать универсальный метод для проверки положения.
When /^I visit the (?:root|home) page$/ do
  visit('/')
end

When /^I visit the countries page$/ do
  visit countries_path
end

When /^I visit the teams page$/ do
  visit teams_path
end

When /^I visit the works page$/ do
  visit works_path
end


When /^I am on the (?:root|home) page$/ do
  visit('/')
end

When /^I am on the countries page$/ do
  visit countries_path
end

When /^I am on the teams page$/ do
  visit teams_path
end

When /^I am on the works page$/ do
  visit works_path
end


When /^I follow "([^\"]*)"$/ do |link|
  click_link(link)
end


When /^I fill in "([^\"]*)" "([^\"]*)" with (.+)$/ do |model, field, value|
  if !model.empty?
    fill_in(model + "[" + field + "]", with: value)
  else
    fill_in(field, with: value)
  end
end

When /^I select (.+) from "([^\"]*)" "([^\"]*)"$/ do |value, model, field|
  select(value, from: model + "[" + field + "]") 
end

When /^I press "([^\"]*)"$/ do |button|
  click_button(button)
end


Then /^I should see (.+)$/ do |text|
  page.should have_content(text)
end

Then /^I should not see (.+)$/ do |text|
  page.should_not have_content(text)
end

Then /^I see content (.+) in table column (.+)$/ do |text, column|
  page.find(".#{column}").should have_content(text)
end