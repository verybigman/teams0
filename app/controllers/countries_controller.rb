# encoding: utf-8
class CountriesController < ApplicationController

  def index
  	@countries = Country.all
  end

  def new
  	@country = Country.new
  end

  def create
  	@country = Country.create(params[:country])
  	if @country.save
  		flash[:notice] = "New country added successfully!"
  		redirect_to action: :index
  	else
  		flash[:error] = "Error! Try again, please." 
  		render :new
  	end
  end

end
