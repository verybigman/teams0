# encoding: utf-8
class TeamsController < ApplicationController

  # Говорим что у нас есть автодополнение по тэгам, названным 'works'.
  # Значения берем из колонки 'name'. Модель 'Tag'.
  # Метод позволяет нам использовать в формах поле с автодополнением.
  autocomplete :works, :name, class_name: 'ActsAsTaggableOn::Tag'

  def index
    # Выбираем список всех стран для фильтра
    @countries = Country.all

    # Метод filter принимает значение 'sort' по умолчанию,  
    # если параметр params[:filter] не передан в запросе. 
    # Параметр устанавливает тип сортировки или фильтрации.
    # Значение 'filter' - установлен фильтр.
    # Значение 'sort' - установлена сортировка.
    # Метод column принимает значение 'cost' по умолчанию, 
    # если параметр params[:column] не передан в запросе.
    # Параметр устанавливает колонку таблицы, по которой
    # будет проводится фильтрация или сортировка.
    # Аналогично устроен метод direction, который отвечает за
    # направление сортировки. По умолчанию 'asc'.
    # Параметр params[:value] принимает значение для фильтров.
    case filter
        when 'filter'
            if column == "works"
                # Уникальная фильтрация для тэгов. 
                # Задействован метод tagged_with из гема acts_as_taggable_on.
                # Получаем список тэгов в виде строки и переводим в массив.
                works = params[:value].split(", ")
                # Выбираем список бригад, которые содержат хотябы один из тэгов
                # на что указывает параметр 'any: true' в запросе.
                # Затем сортируем список бригад по кол-ву вхождений тэгов DESC
                # спасибо методу &
                @teams = Team.tagged_with(params[:value], any: true).sort_by! { |o| -(works & o.work_list).length }
                # Формируем понтовое сообщение для вьюхи. Где указываем по каким тэгам отработал фильтр.
                @msg = "Teams with work #{params[:value].chomp(", ")}"
            else
                # Универсальная фильтрация.
                @teams = Team.where(column + " = " + params[:value])
                # Если у нас идет фильтрация по ключу из внешней таблицы,
                # то название колонки содежит '_id'. Для вывода сообщения во вьюху
                # это не симпотично и стоит удалить '_id' из названия колонки. 
                column_out = column.sub("_id", "")
                # Опять же если фильтрация по ключу из внешней таблицы,
                # то колонка содержит только id, а нам для вывода сообщения
                # нужно знать какое значение соответствует переданному id.
                if column.scan("_id").size > 0
                    value = column_out.classify.constantize.find(params[:value]).name
                else 
                    value = params[:value]
                end
                @msg = "Teams filtered by #{column_out} with #{value}"
            end
        when 'sort'
            @teams = Team.order(column + " " + direction)
            @msg = "Teams sorted by #{column} #{direction}" 
        else
            @teams = Team.all
    end
  end

  def show
  	@team = Team.find(params[:id])
  end

  def new
  	@team = Team.new
  end

  def create 
  	@team = Team.create(params[:team])
    if @team.save!
      flash[:notice] = "New team added successfully!"
      redirect_to action: :index
    else
      flash[:error] = "Error! Try again, please."
      render :new
    end
  end

  def edit
  	@team = Team.find(params[:id])
  end

  def update
    @team = Team.find(params[:id])
    if @team.update_attributes(params[:team])
      flash[:notice] = "Team updated successfully!"
      redirect_to action: :index
    else
      flash[:error] = "Error! Try again, please."
      render :edit
    end
  end

  def filter
      params[:filter] || "sort"
  end

  def column
      params[:column] || "cost"
  end

  def direction
      params[:direction] || "asc"
  end
  
end
