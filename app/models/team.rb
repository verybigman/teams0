class Team < ActiveRecord::Base
  attr_accessible :name, :country_id, :count, :cost, :work_list

  belongs_to :country

  # Каждая бригада может выполнять большой спект работ, которые заданы тэгами.
  acts_as_taggable_on :works

end
