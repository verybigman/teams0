class Country < ActiveRecord::Base
  attr_accessible :name

  # В каждой стране может быть куча бригад.
  has_many :teams
  
end
