module TeamsHelper
	# Вспомогательный метод для создания ссылок, которые подразумевают сортировку по параметру.
	# Как устроена сортировка можно посмотреть в TeamController, метод index.
	def sortable(column, header=nil)
		title = header ||= column.capitalize
		# Главное тут, что устанавливается направление сортировки 'DESC', по умолчанию.
		# Сделал так, потому что при просмотре бригад по умолчанию они отсортированы по 'cost' 'asc',
		# если нажать на заголовок колонки то порядок сортировки измениться на 'DESC'.
		# Если сделать по умолчанию 'ASC', то понадобится два клика для изменения порядка.
		# Потому что первый отсортирует в том же порядке в каком они и были отсортированы.
		direction = column == params[:column] && params[:direction] == "desc" ? "asc" : "desc"
		link_to title, filter: 'sort', column: column, direction: direction
	end
end
