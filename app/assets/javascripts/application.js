//= require jquery
//= require jquery_ujs
//= require_tree .
//= require jquery.ui.autocomplete
//= require autocomplete-rails
//= require bootstrap

$(document).ready(function () {
	$("#show-autocomplete").click(function () {
		$('#autocomplete form').toggleClass("hide");
	});
});


