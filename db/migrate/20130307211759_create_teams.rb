class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name, null: false
      t.integer :country_id
      t.integer :count, default: 0
      t.decimal :cost, precision: 6, scale: 2, default: 0

      t.timestamps
    end
  end
end
